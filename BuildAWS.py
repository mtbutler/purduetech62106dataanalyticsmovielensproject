import logging
import boto3
from botocore.exceptions import ClientError


def create_bucket(bucket_name, region=None):
    # Create bucket
    try:
        if region is None:
            s3_client = boto3.client('s3')
            s3_client.create_bucket(Bucket=bucket_name)
        else:
            s3_client = boto3.client('s3', region_name=region)
            location = {'LocationConstraint': region}
            s3_client.create_bucket(Bucket=bucket_name,
                                    CreateBucketConfiguration=location)
    except ClientError as e:
        logging.error(e)
        return False
    return True

def create_bucket_folder(bucket_name, directory_name):
        # Create folder
    try:
        s3_client = boto3.client('s3')
        s3.put_object(Bucket=bucket_name, Key=(directory_name+'/'))
    except ClientError as e:
        logging.error(e)
        return False
    return True


def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True

CreateMe = 'purdue621moviesdata'
create_bucket(CreateMe)
# to use GLue, you have to put a single file per folder.  If you put all the files in one folder, it reads it like one big table and doesn't really make sense.

create_bucket_folder(CreateMe, 'movies')
create_bucket_folder(CreateMe, 'ratings')
create_bucket_folder(CreateMe, 'links')
create_bucket_folder(CreateMe, 'tags')

#data not included in the repo
upload_file('data/movies.csv', CreateMe)
upload_file('data/ratings.csv', CreateMe)
upload_file('data/links.csv', CreateMe)
upload_file('data/tags.csv', CreateMe)
