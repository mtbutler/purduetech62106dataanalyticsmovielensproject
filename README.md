# PurdueTech62106DataAnalyticsMovieLensProject

Queries and tools for the movieLens data set.

# Notes
This toolkit is a scratch pad area for analyzing the movieLens dataset.  It's organization, while public, is only for the class.

# Requirements
AWS Account with S3, Athena, QuickSight
Excel
LucidCharts

# Data Structure Concepts
https://lucid.app/invitations/accept/13c8ab84-b891-4451-a317-6e9b5bc19b43

# Future Considerations
1. Pivot the tags out like dummy variables and apply to the ratings and movies data tables
2. Run a python lookup script against the URLs to validate they are accurate.