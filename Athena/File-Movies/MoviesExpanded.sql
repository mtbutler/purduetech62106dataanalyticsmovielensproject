CREATE OR REPLACE VIEW vmoviesextended AS 
SELECT
  *
, "replace"("replace"("element_at"("regexp_extract_all"("title", '[(][1-2][0-9][0-9][0-9][)]'), 1), '('), ')') "releasedate2"
, (CASE WHEN ("index"("genres", 'Action') > 0) THEN 1 ELSE 0 END) "isAction"
, (CASE WHEN ("index"("genres", 'Adventure') > 0) THEN 1 ELSE 0 END) "isAdventure"
, (CASE WHEN ("index"("genres", 'Animation') > 0) THEN 1 ELSE 0 END) "isAnimation"
, (CASE WHEN ("index"("genres", 'Children') > 0) THEN 1 ELSE 0 END) "isChildrens"
, (CASE WHEN ("index"("genres", 'Comedy') > 0) THEN 1 ELSE 0 END) "isComedy"
, (CASE WHEN ("index"("genres", 'Crime') > 0) THEN 1 ELSE 0 END) "isCrime"
, (CASE WHEN ("index"("genres", 'Documentary') > 0) THEN 1 ELSE 0 END) "isDocumentary"
, (CASE WHEN ("index"("genres", 'Drama') > 0) THEN 1 ELSE 0 END) "isDrama"
, (CASE WHEN ("index"("genres", 'Fantasy') > 0) THEN 1 ELSE 0 END) "isFantasy"
, (CASE WHEN ("index"("genres", 'Film-Noir') > 0) THEN 1 ELSE 0 END) "isFilmNoir"
, (CASE WHEN ("index"("genres", 'Horror') > 0) THEN 1 ELSE 0 END) "isHorror"
, (CASE WHEN ("index"("genres", 'Musical') > 0) THEN 1 ELSE 0 END) "isMusical"
, (CASE WHEN ("index"("genres", 'Mystery') > 0) THEN 1 ELSE 0 END) "isMystery"
, (CASE WHEN ("index"("genres", 'Romance') > 0) THEN 1 ELSE 0 END) "isRomance"
, (CASE WHEN ("index"("genres", 'Sci-Fi') > 0) THEN 1 ELSE 0 END) "isSciFi"
, (CASE WHEN ("index"("genres", 'Thriller') > 0) THEN 1 ELSE 0 END) "isThriller"
, (CASE WHEN ("index"("genres", 'War') > 0) THEN 1 ELSE 0 END) "isWar"
, (CASE WHEN ("index"("genres", 'Western') > 0) THEN 1 ELSE 0 END) "isWestern"
, (CASE WHEN ("index"("genres", '(no genres listed)') > 0) THEN 1 ELSE 0 END) "isNoGenre"
FROM
  moviesdatabase.moviesmovies

