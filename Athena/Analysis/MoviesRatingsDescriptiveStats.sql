CREATE OR REPLACE VIEW vmoviesratings AS 
SELECT
  "m"."movieid"
, "min"("rating") "MovieRatingsMin"
, "max"("rating") "MovieRatingsMax"
, "avg"("rating") "MovieRatingsAverage"
, "stddev"("rating") "MovieRatingStdDev"
, "count"(1) "MovieRatingsCount"
FROM
  (moviesdatabase.moviesmovies m
LEFT JOIN moviesdatabase.moviesratings r ON ("m"."movieid" = "r"."movieid"))
GROUP BY "m"."movieid"

