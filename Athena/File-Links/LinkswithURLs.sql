select *, 
concat('http://www.imdb.com/title/tt0', cast(imdbid as varchar)) as IMDBLink,
concat('https://www.themoviedb.org/movie/', cast(tmdbid as varchar)) as TMDBLink

from moviesdatabase.movieslinks l full outer join moviesdatabase.moviesmovies m on l.movieid = m.movieid
